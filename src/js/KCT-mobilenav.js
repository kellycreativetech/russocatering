// off-canvas open and close
document.querySelectorAll(".open-panel").forEach(function (panel) {
	panel.addEventListener("click", function () {
		document.documentElement.classList.toggle("openNav");
		panel.classList.toggle("is-active");
	});
});

// Helper function for slideToggle equivalent
function slideToggle(element) {
	if (element.style.display === "none" || element.style.display === "") {
		element.style.display = "block";
		element.style.transition = "max-height 0.3s ease-in-out";
		element.style.maxHeight = element.scrollHeight + "px";
	} else {
		element.style.maxHeight = "0";
		setTimeout(function () {
			element.style.display = "none";
		}, 300); // Match with the CSS transition time
	}
}

// display/hide mobile submenus
function mobile_nav_ui() {
	var mobileNavItems = document.querySelectorAll(".mobile-nav > ul > li");

	mobileNavItems.forEach(function (li) {
		var hasSubmenu = li.querySelector("ul");
		var link = li.querySelector("a");

		if (hasSubmenu && link) {
			li.classList.add("has-child");

			link.addEventListener("click", function (e) {
				// Prevent default if link doesn't point to #
				if (
					link.getAttribute("href") !== "#" &&
					!link.classList.contains("clicked-once")
				) {
					e.preventDefault();

					// Append the cloned link to the submenu
					var linkhref = link.getAttribute("href");
					var newListItem = document.createElement("li");
					newListItem.classList.add("prepended");

					var newLink = document.createElement("a");
					newLink.setAttribute("href", linkhref);
					newLink.textContent = link.cloneNode(true).textContent;

					newListItem.appendChild(newLink);
					hasSubmenu.insertBefore(newListItem, hasSubmenu.firstChild);
				}

				// Add clicked-once class if it doesn't have it
				if (!link.classList.contains("clicked-once")) {
					link.classList.add("clicked-once");
				}

				// Toggle submenu
				link.classList.toggle("open-subs");
				li.classList.toggle("down");
				slideToggle(hasSubmenu);
			});
		}
	});
}

// Fire the nav function on load
document.addEventListener("DOMContentLoaded", function () {
	mobile_nav_ui();
});
