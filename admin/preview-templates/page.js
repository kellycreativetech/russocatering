import htm from "https://unpkg.com/htm?module";

const html = htm.bind(h);

// Preview component for a Page
const Page = createClass({
  render() {
    const entry = this.props.entry;
    const galleryImgs = entry.getIn(["data", "galleryImages"]);
    const metaTitle = entry.getIn(["data", "pagemeta", "title"]);
    const metaDesc = entry.getIn(["data", "pagemeta", "description"]);
    const metaImg = entry.getIn(["data", "pagemeta", "meta_image"]);
    const quoteName = entry.getIn(["data", "quotename"]);
    const quoteCompany = entry.getIn(["data", "quotecompany"]);
    const mobileimage = entry.getIn(["data", "mobileimage"]);
    const image = entry.getIn(["data", "image"]);



    return html`
        ${quoteName ?
          html`
          <section className="masthead">
            <img className="masthead--img" src="${entry.getIn(['data', 'image'])}" />
            <img className="masthead--img-mobile" src="${entry.getIn(['data', 'mobileimage'])}" />
            <div className="l-container v-pad">
            <blockquote className="masthead--quote l-one-half l-last break-at-skinny">
                ${this.props.widgetFor("body")}
                <cite>
                  ${entry.getIn(['data', 'quotename'])}<br />
                  <span className="cite-meta">${entry.getIn(['data', 'quotecompany'])}</span>
                </cite>
            </blockquote>
            </div>
          </section>`

        : html`<div className="l-container">
          <div className="l-two-thirds">
            <h1>${entry.getIn(["data", "title"], null)}</h1>
            ${this.props.widgetFor("body")}
          </div>
          <div className="l-one-third l-last">
            <img src="${entry.getIn(['data', 'image'])}" />
          </div>
        </div>
        `}


        ${galleryImgs ?
          html`
          <section className="home-callout">
            <div className="home-callout--wrap">
              <div className="l-container">
                <ul className="home-callout--list">

                ${this.props.widgetsFor('galleryImages').map(
                  img =>
                    html`
                      <li className="home-callout--item">
                        <a href="${img.getIn(['data', 'link'])}" className="home-callout--link" data-fancybox>
                          <img className="home-callout--img" src="${img.getIn(['data', 'image']) + '-/scale_crop/800x800/'}" alt="" />
                          <span className="home-callout--title">${img.getIn(['data', 'title'])}</span>
                        </a>
                      </li>
                    `)}
                </ul>
              </div>
            </div>
          </section>
          `
          : null}

      <div className="social-preview">
        <p className="social-preview--label">Search Engine Listing Preview</p>
        <div className="fake-google">
          <h3><a>
            ${metaTitle ?
              this.props.widgetsFor('pagemeta').getIn(['data', 'title'], null) :
              entry.getIn(["data", "title"], null) + " • Russo Catering, Zanesville, Ohio"}
          </a></h3>
          <p><a className="link">russocatering.com${entry.getIn(['data', 'permalink'])}</a></p>
          <p>
            ${metaDesc ?
              this.props.widgetsFor('pagemeta').getIn(['data', 'description'], null) :
              'Search engine listings nearly always display a relevant snippet of text from the search. However if the page is sparse on content, this field may sometimes be used.'
              }
          </p>
        </div>

        <p className="social-preview--label">Social Media Card Preview</p>
        <div className="fake-facebook">
          <img
            className="social-preview--card-img"
            src="${metaImg ?
                   this.props.widgetsFor('pagemeta').getIn(['data', 'meta_image']) + '-/resize/1200x627' :
                   '/dist/img/social-bg.jpg'
                 }"
          />
          <div className="wrap">
          <h3><a>${metaTitle ?
            this.props.widgetsFor('pagemeta').getIn(['data', 'title'], null) :
            entry.getIn(["data", "title"], null) + " • Russo Catering, Zanesville, Ohio" }
          </a></h3>

          <p className="desc">
            ${metaDesc ?
              this.props.widgetsFor('pagemeta').getIn(['data', 'description'], null) :
              'Search engine listings nearly always display a relevant snippet of text from the search. However if the page is sparse on content, this field may sometimes be used.'
              }
          </p>

          <p className="link">russocatering.com${entry.getIn(['data', 'permalink'])}</p>
          </div>




        </div>
      </div>
    `;
  }
});

export default Page;
