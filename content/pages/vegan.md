---
title: Vegan Catering
date: 2024-09-21T21:03:39.963Z
permalink: /vegan-wedding-catering/
image: /dist/img/food/veggies.jpg
pagemeta:
  title: Vegan Catering Columbus, OH
  description: Vegan Catering Columbus, OH
---

The culinary professionals at Russo's Catering & Events have developed an extensive Vegan menu that provides many delicious Vegan options. In addition to Vegan cuisine, we can accommodate all dietary requirements, such as vegetarian, dairy free, lactose free and gluten free cuisines.  We offer a fully customizable experience. Please inquire.
