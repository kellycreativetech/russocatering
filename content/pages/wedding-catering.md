---
title: Wedding Catering
date: 2024-09-21T20:59:06.677Z
permalink: /wedding-catering/
image: /dist/img/img_1294.jpeg
pagemeta:
  title: Wedding Catering Columbus, OH
  description: Wedding catering Columbus, OH
---
* Wedding Receptions
* Bridal Showers
* Rehearsal Dinners

<div>
<a class="btn" href="{{ metadata.on_site_menu | url }}">See On Site Catering Catalog</a><br>
</div>