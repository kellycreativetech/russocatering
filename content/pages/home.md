---
title: Russo's Catering & Events
date: 2024-09-21T21:11:57.262Z
permalink: /
layout: home.njk
galleryImages:
  - title: Weddings
    image: /dist/img/food/lamb.jpg
    link: wedding-catering
  - title: Corporate
    image: /dist/img/food/40A0B652-9E21-4F18-A203-81927A801045.jpeg
    link: corporate-events
  - title: Friends and Family Gatherings
    image: /dist/img/food/family-home.jpg
    link: friends-family
  - title: Carry Out
    image: /dist/img/img_0810.jpeg
    link: carry-out
pagemeta:
  title: Top Caterer Columbus, OH
  description: Top Caterer Columbus, OH
---

Russo’s catering helped make my company Christmas party a huge success! The food was exceptional, and Anthony’s team was personable and professional. I’ll be hiring them again next year, for sure!
