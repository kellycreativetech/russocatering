---
title: Corporate Event Catering
date: 2024-09-21T21:00:56.200Z
permalink: /corporate-events/
image: /dist/img/food/shrimp.jpg
carryout: false
pagemeta:
  title: Corporate catering Columbus, OH
  description: Corporate catering Columbus, OH
---
* Holiday Parties
* Employee Appreciation
* Corporate Events