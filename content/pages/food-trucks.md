---
title: Carry Out Catering
date: 2024-09-21T21:08:59.364Z
permalink: /carry-out/
image: /dist/img/img_0809.jpeg
pagemeta:
  title: Caterer Columbus, OH
  description: Caterer Columbus, OH
---
* At Least 1-Week Advance Notice Required
* $500 Food Order Minimum
* Delivery and Set-Up Subject to Availability
* Disposable Chafing Sets and Hot Holding Boxes Available
* Disposable Tableware and Serving Utensils Available

<div>
<a class="btn" href="{{ metadata.carry_out_catering_menu | url }}">See Carry Out Catering Catalog</a><br>
</div>