---
title: About us
date: 2017-01-01T00:00:00.000Z
quotename: Rob B.
quotecompany: 2020 Columbus Winter Beerfest Connoisseur Reception Attendee
quotename2: Alissa W.
permalink: /about/
quotecompany2: Wedding Client
image: /dist/img/food/B9856CC8-1BF7-4544-99A7-22CC0D2169AF.jpeg
pagemeta:
  title: Best Caterer Columbus, OH
  description: Best Caterer Columbus, OH
quote2: Russo's Catering & Events catered our wedding. They were friendly,
  attentive, accommodating and professional. They were very easy to work with
  and the food was AMAZING!!
quote: Russo's Catering & Events provided the best food that my wife and I have
  ever experienced in the last 6 years of coming to this event.  Definitely
  would love to see them do this every year!
---
Russo’s Catering & Events is an experienced, professional catering company serving Greater Central and Southeast Ohio. We offer a wide variety of fresh and vibrant cuisine and catering packages to suit every taste and style. And, we can accommodate a number of dietary requirements, such as vegan, nut allergies and gluten free.

Our culinary professionals can prepare hors d’oeuvres and decadent sweet treats to add the finishing touches to a wonderfully catered gourmet lunch or dinner. Additional offerings include live-action meat-carving stations, bar and beverage services, wood fired pizza and hamburger food trucks, and wedding cakes.

The team at Russo’s Catering & Events is dedicated to making your event special and memorable. Our catering experts deliver with excellence, class and elegance.

Contact us today to start planning your next event.